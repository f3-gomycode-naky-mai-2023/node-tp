var http = require('http')
var fs = require('fs')

var app = http.createServer(function(req, res) {

    fs.readFile('index.html', function(err, data) {
        res.writeHead(200, { 'Content-Type': 'text/html' })
        res.write(data)
        res.end()
    })

})
app.listen(4000)
console.log("server is started on http://localhost:4000")